import moment from 'moment';

import { ITask } from './models';

const isDone = (task: ITask) => !!task.done;

const isActive = (task: ITask) => !isDone(task) && !!task.started;

const isOverdue = (task: ITask) => !isDone(task) && moment().isAfter(task.due_at);

const getStatus = (task: ITask) => {
	let status = 'default';
	if (isDone(task)) {
		return status = 'done';
	} else if (isActive(task)) {
		status = 'active';
	}

	if (isOverdue(task)) {
		status += '_overdue';
	}

	return status;
};

const rules = {
	isActive,
	isDone,
	isOverdue,
	getStatus
};

export default rules;
