import { IUser } from './models';

const getFullname = (user: IUser) => user.first_name + ' ' + user.last_name;

const rules = {
	getFullname,
};

export default rules;
