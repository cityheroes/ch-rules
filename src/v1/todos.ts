import { ITodo } from './models';

const isDone = (todo: ITodo) => !!todo.done_at;

const rules = {
	isDone
};

export default rules;
