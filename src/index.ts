import v1Rules from './v1/index';
import v2Rules from './v2/index';

const rules = {
	v1: v1Rules,
	v2: v2Rules
};

export = rules;
