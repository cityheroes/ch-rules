interface IUser {
	ref_id: number;
	email: string;
	password: string;
	first_name: string;
	last_name: string;
	active?: boolean;
	phone_number?: string;
}

interface IGeolocation {
	type: string;
	coordinates: [number];
	accuracy?: number;
	speed?: number;
	heading?: number;
	altitude_accuracy?: number;
}

interface IMomentStamp {
	geolocation: IGeolocation;
	created_at: Date;
}

interface ITodo {
	text: string,
	done?: IMomentStamp
}

interface ITask {
	name: string;
	description?: string;
	started?: IMomentStamp;
	done?: IMomentStamp;
	assigned_user: IUser;
	geolocation: IGeolocation;
	due_at: Date;
	voided_at?: Date;
	voided_by?: IUser;
	voided_reason?: string;
}

export {
	IUser,
	IGeolocation,
	IMomentStamp,
	ITask,
	ITodo
};
