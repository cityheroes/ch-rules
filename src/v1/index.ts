import TaskRules from './tasks';
import UserRules from './users';
import TodoRules from './todos';

const rules = {
	tasks: TaskRules,
	users: UserRules,
	todos: TodoRules
};

export = rules;
