import { ITodo } from './models';

const isDone = (todo: ITodo) => !!todo.done;

const rules = {
	isDone
};

export default rules;
