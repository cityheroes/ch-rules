interface IUser {
	ref_id: number;
	email: string;
	password: string;
	first_name: string;
	last_name: string;
	active?: boolean;
}

interface ITodo {
	text: string;
	done_at: Date;
	done_lat?: number;
	done_lng?: number;
}

interface ITask {
	name: string;
	description?: string;
	lat?: number
	lng?: number;
	started_at: Date;
	started_lat?: number
	started_lng?: number;
	done_at: Date;
	done_lat?: number
	done_lng?: number;
	assigned_user: IUser;
	due_at: Date;
	etoc: number;
	voided_at?: Date;
	voided_by_user?: IUser;
	voided_reason?: string;
}

export {
	IUser,
	ITask,
	ITodo
};
